'use strict'

const gulp = require('gulp')
const webserver = require('gulp-webserver')
const sass = require('gulp-sass')
const sourcemaps = require('gulp-sourcemaps')
const eslint = require('gulp-eslint')

const GLOB_SCSS = 'app/scss/**/*.scss'
const GLOB_JS = 'app/js/**/*.js'

gulp.task('serve', () => {
    gulp.src('app').pipe(webserver({ livereload: true }))
})

gulp.task('scss', () => {
    return gulp.src(GLOB_SCSS)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('app/css'))
})

gulp.task('js', () => gulp.src(GLOB_JS).pipe(eslint.format()))

gulp.task('watch', () => {
    gulp.watch(GLOB_SCSS, ['scss'])
    gulp.watch(GLOB_JS, ['js'])
})