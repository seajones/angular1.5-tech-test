'use strict'

const API_KEY = '81e1dfc180334e681b5594c0f8d21fc0'
angular.module('weatherr', [])
    .controller('MainCtrl', function ($scope, $http) {
        this.name = 'Weatherr'

        this.cities = [
            {
                name: 'London',
                loading: true,
                id: '2643743'
            },
            {
                name: 'Cardiff',
                loading: true,
                id: '2653822'
            },
            {
                name: 'Amsterdam',
                loading: true,
                id: '2759794'
            },
            {
                name: 'Milan',
                loading: true,
                id: '6542283'
            },
            {
                name: 'Bristol',
                loading: true,
                id: '2654675'
            },
        ]

        this.cities.forEach((city, index) => {
            $http.get(currentForecastURL(city.id, API_KEY))
                .then(
                    data => {
                        this.cities[index].description = data.data.weather[0].description
                        this.cities[index].temperature = kelvinToCelsius(data.data.main.temp)
                        this.cities[index].tempMin = kelvinToCelsius(data.data.main.temp_min)
                        this.cities[index].tempMax = kelvinToCelsius(data.data.main.temp_max)
                        this.cities[index].windSpeed = data.data.wind.speed
                        this.cities[index].iconUrl = `https://openweathermap.org/img/w/${data.data.weather[0].icon}.png`
                        this.cities[index].windDirection = degToCompass(data.data.wind.deg)
                        this.cities[index].loading = false

                        $http.get(forecastURL(city.id, API_KEY))
                            .then((hours) => {
                                const fiveHours = hours.data.list.slice(0,5)
                                this.cities[index].hours = fiveHours.map((weatherData, index) => {
                                    const avgTemp = fiveHours.map((hour) => Number(kelvinToCelsius(hour.main.temp))).reduce((prev, curr) => prev + curr) / 5
                                    return {
                                        description: weatherData.weather[0].description,
                                        icon: `https://openweathermap.org/img/w/${weatherData.weather[0].icon}.png`,
                                        temperature: kelvinToCelsius(weatherData.main.temp),
                                        title: `in ${index * 3} hours`,
                                        chartheight: 100 * (kelvinToCelsius(weatherData.main.temp) / avgTemp)/2
                                    }
                                })
                            })
                    },
                    error => {
                        this.cities[index].errored = true
                        console.error(error)
                    }
                )
        })
        
    })

function currentForecastURL(cityString, apiKey) {
    return `https://api.openweathermap.org/data/2.5/weather?id=${cityString}&appid=${apiKey}`
}

function forecastURL(cityString, apiKey) {
    return `https://api.openweathermap.org/data/2.5/forecast?id=${cityString}&appid=${apiKey}`
}

function kelvinToCelsius (degreesL) {
    return Number(degreesL - 273.15).toFixed(1)
}

function degToCompass(degrees) {
    return ['N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW', 'N'][Math.round(degrees / 11.25 / 2)]
}